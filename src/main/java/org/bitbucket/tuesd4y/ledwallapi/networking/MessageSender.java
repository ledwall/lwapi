package org.bitbucket.tuesd4y.ledwallapi.networking;

import org.bitbucket.tuesd4y.ledwallapi.entity.Block;
import org.bitbucket.tuesd4y.ledwallapi.entity.Colour;
import org.bitbucket.tuesd4y.ledwallapi.entity.LedWallMode;
import org.bitbucket.tuesd4y.ledwallapi.entity.SpecialSignal;
import org.json.JSONArray;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.List;

/**
 * An object that can be used to send a given message over UDP-protocol.
 * Has to be created using an IP-address and a port
 * @author stoez
 * @version 3.0b 5/28/2016
 */
public abstract class MessageSender {

    private int port;
    private InetAddress ipAddress;

    protected DatagramSocket socket;

    /**
     * Converts a list of blocks to a string and passes that string to the {@code sendRawMessage}-function
     * @param blocks
     *      The blocks which should be sent to the LedWall as JSON-objects using the {@code b.toJsonObject}-function
     */
    public void sendBlocks(List<Block> blocks){
        JSONArray arr = new JSONArray();
        for (Block  b: blocks) {
            arr.put(b.toJsonObject());
        }
        sendRawMessage(arr.toString());
    }

    /**
     * Private method sending a given string to the LedWall
     * @param message
     *      The Message-string that will be sent to the LedWall
     */
    protected abstract void sendRawMessage(String message);

    /**
     * Converts a block to a string and passes that string to the {@code sendRawMessage}-function
     * @param block
     *      The blocks which should be sent to the LedWall as a Json-Object using the {@code b.toJsonObject}-function
     */
    public void sendBlock(Block block) {
        JSONArray arr = new JSONArray();
        arr.put(block.toJsonObject());
        sendRawMessage(arr.toString());
    }

    /**
     * Passes a mode change to the {@code sendRawMessage}-function
     * @param mode
     *      The mode that should be sent
     */
    public void sendModeChange(LedWallMode mode) {
        String msg = "{'mode':" + mode.getVal()+"}";
        sendRawMessage(msg);
    }

    /**
     * Passes a string message to the {@code sendRawMessage}-function
     * @param text
     *      The text-message that should be sent
     */
    public void sendMessage(String text) {
        sendRawMessage("{'msg':'" + text + "'}");
    }

    /**
     * Passes a special signal to the {@code sendRawMessage}-function
     * @param s
     *      The special signal that should be sent
     */
    public void sendSpecialSignal(SpecialSignal s) {
        sendRawMessage("{'sig':" + s.getVal() + "}");
    }

    /**
     * Passes a message and a color to the {@code sendRawMessage}-function
     * @param text
     *      The text-message that should be sent
     * @param c
     *      The color the message will be displayed in
     */
    public void sendMessage(String text, Colour c) {
        sendRawMessage("{'msg':'" + text + "', 'fgcol':" + c.toJsonObject().toString() + "}");
    }

    public MessageSender(InetAddress ipAddress, int port) throws SocketException {
        this.ipAddress = ipAddress;
        this.port = port;
        this.socket = new DatagramSocket();
        System.out.println("Created new MessageSender on " + ipAddress.toString() + ":" + port);
    }

    public MessageSender() throws SocketException {
        this.socket = new DatagramSocket();
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setIpAddress(InetAddress ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getPort() {
        return port;
    }

    public InetAddress getIpAddress() {
        return ipAddress;
    }
}
