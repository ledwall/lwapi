package org.bitbucket.tuesd4y.ledwallapi.entity;

/**
 * Enum representing the different modes of the LedWall
 * @author stoez
 * @version 2.0 5/31/2016
 */
public enum LedWallMode {
    GAME(0),
    MESSAGE(1),
    MINESWEEPER(4);

    private int val;
    LedWallMode(int val) {
        this.val = val;
    }

    public int getVal() {
        return val;
    }
}
