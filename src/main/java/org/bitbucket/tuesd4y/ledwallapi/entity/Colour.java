package org.bitbucket.tuesd4y.ledwallapi.entity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A Colour that can be sent to the LedWall. A color can be created either by using an Android Color
 * Integer or by parsing it from a RGB-value
 * @author stoez
 * @version 3.0b 5/28/2016
 */
public class Colour {
    private int r;
    private int g;
    private int b;

    /**
     * Creates a new colour from the given RGB-values
     * @param r
     *      the 'red' value of the colour to be created
     * @param g
     *      the 'green' value of the colour to be created
     * @param b
     *      the 'blue' value of the colour to be created
     */
    public Colour(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    /**
     * Creates a new colour from the given Android Color Integer
     * @param c
     *      The Android colour integer of the colour to be created
     */
    public Colour(int c){
        r = red(c);
        g = green(c);
        b = blue(c);
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public int getG() {
        return g;
    }

    public void setG(int g) {
        this.g = g;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    /**
     * Converts the colour to a json object.
     * @return
     *      A json object containing the values of the colour
     */
    public JSONObject toJsonObject(){
        JSONObject o = new JSONObject();
        try {
            o.put("r", r);
            o.put("g", g);
            o.put("b", b);
        } catch (JSONException e) {
            e.printStackTrace();
            return new JSONObject();
        }
        return o;
    }

    public static int red(int colour) {
        return (colour >> 16) & 0xFF;
    }

    public static int green(int colour) {
        return (colour >> 8) & 0xFF;
    }
    public static int blue(int colour) {
        return  colour & 0xFF;
    }
}
