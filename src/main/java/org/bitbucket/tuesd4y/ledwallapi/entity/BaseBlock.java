package org.bitbucket.tuesd4y.ledwallapi.entity;

import org.json.JSONObject;

/**
 * BaseBlock provides a simple implementation for converting a block to a jsonObject
 * @author stoez
 * @version 1.0 3/20/2016
 * @see Block
 */
public abstract class BaseBlock implements Block {
    private final String TAG = getClass().getSimpleName();

    @Override
    public JSONObject toJsonObject() {
        JSONObject b = new JSONObject();
        try{
            b.put("row", getRow());
            b.put("col", getColumn());
            b.put("colour", getType()+1);
        } catch (Exception ex){
            //throw new JSONException("failed to add data to json object");
            //no idea when this happens so...
            System.err.println("toJsonObject: BaseBlock cannot be converted to JsonObject");
            ex.printStackTrace();
        }
        return b;
    }
}
