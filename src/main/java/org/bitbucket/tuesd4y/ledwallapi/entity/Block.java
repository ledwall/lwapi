package org.bitbucket.tuesd4y.ledwallapi.entity;

import org.json.JSONObject;

/**
 * Simple interface easing the conversion from blocks in different games to a jsonObject containing
 * values for color, row and column to display on the LedWall
 * @author stoez
 * @version 1.0 3/17/2016
 */
public interface Block {
    int getRow();
    int getColumn();
    int getType();

    /**
     * converts a block to a json object which is readable by the LedWall
     * @return
     *      a jsonObject containing values for row, col and colour
     */
    JSONObject toJsonObject();
}

