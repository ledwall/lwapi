package org.bitbucket.tuesd4y.ledwallapi.entity;

/**
 * Represents a special signal that can be sent to the LedWall
 * @author stoez
 * @version 1.0 3/29/2016
 */
public enum SpecialSignal {
    SNAKE_IS_DEAD(0),
    CONNECT_4_IS_OVER(1);

    private final int val;

    SpecialSignal(int val) {
        this.val = val;
    }

    public int getVal() {
        return val;
    }
}
